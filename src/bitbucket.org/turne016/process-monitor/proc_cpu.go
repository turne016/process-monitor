package main

import (
    "time"
)


type ProcessCpu struct {
    StartTime time.Time
    User uint64
    Sys uint64
    Total uint64
    LastTime time.Time
    Percent float64
    Pid uint64
    nativeData *SigarProcessCpu
} 

type ProcessMemory struct {
    Size float64
    PageFaults uint64
    Pid uint64
    nativeData *SigarProcessMemory
}

func (cpu *ProcessCpu) Gather() {
    GatherProcessCpu(cpu.Pid, cpu)
}

func (memory *ProcessMemory) Gather() {
    GatherProcessMemory(memory.Pid, memory)
}



type ProcessInfo struct {
    Name string
    Cpu *ProcessCpu
    Memory *ProcessMemory
}

func (info *ProcessInfo) Update() {
   info.Cpu.Gather() 
   info.Memory.Gather()
}

type SystemCpu struct {
    User uint64
    Sys uint64
    Total uint64
    Percent float64
}

type SystemMemory struct {
    Ram float64
    Total float64
    Used float64 
    Free float64
    Actual_used float64
    Actual_free float64
    Used_percent float64
    Free_percent float64
}

    
