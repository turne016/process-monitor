package main

import (
    "unsafe"
    "syscall"
    "fmt"
    "os"
    "reflect"
    "time"
    "bytes"
)

const MEGABYTE = 1024 * 1024

type SigarHandle *uintptr 

type SigarProcList struct {
    number   uint32
    size     uint32
    data     uintptr
}

func toMegabyte(byteCount uint64) float64 {
    return float64(byteCount)/float64(MEGABYTE)
}

type SigarApiInfo struct {
    functionHandles map[string]uintptr 
    sigarLib syscall.Handle
    sigarInstance SigarHandle
}

func (info *SigarApiInfo) HandleFor(method string) (uintptr) {
    return info.functionHandles[method]
}

var sigarInfo = new(SigarApiInfo)


func init() {
    sigarMethods := []string { 
        "sigar_mem_get",
        "sigar_proc_list_get",
        "sigar_proc_list_destroy",
	"sigar_proc_cpu_get",
        "sigar_proc_mem_get",
        "sigar_proc_state_get",
        "sigar_proc_exe_get",
    }
     var sysCallError error
     sigarInfo.functionHandles = make(map[string]uintptr)
     sigarLib, sysCallError := syscall.LoadLibrary("sigar-amd64-winnt.dll")
     if(sysCallError != nil) {
        fmt.Println("Error opening sigar: ", sysCallError);
	os.Exit(1)
     }
     sigarInfo.sigarLib = sigarLib
     openSigarFunc , openError := syscall.GetProcAddress(sigarLib, "sigar_open")
     if(openError != nil) {
        fmt.Println("Error opening sigar: ", openError);
	os.Exit(1)
     }

     var funcError error
     for _, method := range(sigarMethods) {
         sigarInfo.functionHandles[method], funcError = syscall.GetProcAddress(sigarInfo.sigarLib, method)
         if(funcError != nil) {
             fmt.Println("Error opening function: ", funcError);
    	     os.Exit(1)
	 }
     }

     var newInstance SigarHandle
     sigarInfo.sigarInstance = newInstance
     syscall.Syscall(uintptr(openSigarFunc), 1, uintptr(unsafe.Pointer(&sigarInfo.sigarInstance)), 0,0)
}

type SigarSystemMemory struct {
    Ram uint64
    Total uint64
    Used uint64 
    Free uint64
    Actual_used uint64
    Actual_free uint64
    Used_percent float64
    Free_percent float64

}
func GetSystemMemory() *SystemMemory {
     var memory SigarSystemMemory
     syscall.Syscall(sigarInfo.HandleFor("sigar_mem_get"), 2, 
              uintptr(unsafe.Pointer(sigarInfo.sigarInstance)), uintptr(unsafe.Pointer(&memory)),0)
     return &SystemMemory{toMegabyte(memory.Ram), 
                         toMegabyte(memory.Total), 
			 toMegabyte(memory.Used),
                         toMegabyte(memory.Free), 
			 toMegabyte(memory.Actual_used),
			 toMegabyte(memory.Actual_free),
	                 memory.Used_percent ,
			 memory.Free_percent}
}

func ProcessList() []uint64 {
     var procList SigarProcList
     syscall.Syscall(sigarInfo.HandleFor("sigar_proc_list_get"), 2, 
             uintptr(unsafe.Pointer(sigarInfo.sigarInstance)), uintptr(unsafe.Pointer(&procList)), 0)

     // some manipulation to allow us to reference an array built by the library call
     var result []uint64
     h := (*reflect.SliceHeader)(unsafe.Pointer(&result))
     h.Data = uintptr(unsafe.Pointer(procList.data))
     h.Len  = int(procList.number)
     h.Cap  = int(procList.size)
     return result
}

type SigarProcessMemory struct {
    size uint64
    resident uint64
    share uint64
    minorFaults uint64
    majorFaults uint64
    pageFaults uint64
}

func GatherProcessMemory(pid uint64, procMemory *ProcessMemory) {
     syscall.Syscall(sigarInfo.HandleFor("sigar_proc_mem_get"), 3, 
             uintptr(unsafe.Pointer(sigarInfo.sigarInstance)), uintptr(pid), uintptr(unsafe.Pointer(procMemory.nativeData)))
     procMemory.Pid = pid
     procMemory.Size = toMegabyte(procMemory.nativeData.resident)
     procMemory.PageFaults = procMemory.nativeData.pageFaults
}

func GetProcessMemory(pid uint64) *ProcessMemory {
     var procMemory ProcessMemory
     var sigarMemory SigarProcessMemory
     procMemory.nativeData = &sigarMemory
     GatherProcessMemory(pid, &procMemory)
     return &procMemory
}

type SigarProcessCpu struct {
    StartTime uint64
    User uint64
    Sys uint64
    Total uint64
    LastTime uint64
    Percent float64
} 

func GatherProcessCpu(pid uint64, procCpu *ProcessCpu) {

     syscall.Syscall(sigarInfo.HandleFor("sigar_proc_cpu_get"), 3, 
             uintptr(unsafe.Pointer(sigarInfo.sigarInstance)), uintptr(pid), uintptr(unsafe.Pointer(procCpu.nativeData))) 
     procCpu.Pid = pid
     procCpu.StartTime = time.Unix(int64(procCpu.nativeData.StartTime/1000),0)
     procCpu.User = procCpu.nativeData.User
     procCpu.Sys = procCpu.nativeData.Sys
     procCpu.Total = procCpu.nativeData.Total
     procCpu.LastTime = time.Unix(int64(procCpu.nativeData.LastTime/1000),0)
     procCpu.Percent = procCpu.nativeData.Percent * 100

}

func GetProcessCpu(pid uint64) *ProcessCpu {
     var procCpu ProcessCpu
     procCpu.nativeData = new(SigarProcessCpu)
     GatherProcessCpu(pid, &procCpu)
     return &procCpu
}

type SigarProcState struct {
    name [128]byte
    state byte
    ppid uint64 
    tty  int 
    priority int
    nice int
    processor int
    thread uint64
}

type SigarProcessExe struct {
    name [4096+1]byte
    cwd  [4096+1]byte
    root [4096+1]byte
}

func GetProcessExe(pid uint64) string {
    var sigarExe SigarProcessExe
    syscall.Syscall(sigarInfo.HandleFor("sigar_proc_exe_get"), 3, 
             uintptr(unsafe.Pointer(sigarInfo.sigarInstance)), uintptr(pid), uintptr(unsafe.Pointer(&sigarExe))) 
    nullIndex := bytes.IndexByte(sigarExe.name[:],0)
    return string(sigarExe.name[:nullIndex])

}

func GetProcessInfo(pid uint64) *ProcessInfo {
    name := GetProcessExe(pid)
    cpu := GetProcessCpu(pid)
    memory := GetProcessMemory(pid)
    return &ProcessInfo{name, cpu, memory}
}

func GetSystemCpuByInterval(secs int) float64 {

    pidList := ProcessList()
    procArray := make([]*ProcessInfo, 0, len(pidList)) 
    for _, pid := range(pidList) {
        procArray = append(procArray, GetProcessInfo(pid))
    }
    time.Sleep(time.Duration(secs) * time.Second)

    var percent float64 

    for _, procInfo := range(procArray) {
       procInfo.Update()
       percent += procInfo.Cpu.Percent
    }
    return percent
}





