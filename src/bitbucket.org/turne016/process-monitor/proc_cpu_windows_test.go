package main

import (
    "testing"
    "fmt"
    "os"
    "time"
)

func TestMemory(test *testing.T) {
    result := Memory()
    if(result.Ram == 0) {
        test.Errorf("It didn't work")
    }

}

func TestProcessList(test *testing.T) {
    result := ProcessList()
    if(result == nil) {
        test.Errorf("It didn't work")
    }
}

func TestProcessMemory(test *testing.T) {
    testPid := os.Getpid()
    memory := GetProcessMemory(uint64(testPid))
    if(memory.Size == 0 || memory.PageFaults == 0) {
        fmt.Errorf("Error getting process memory")
    }
    time.Sleep(2000 * time.Millisecond)
    memory.Gather()
    if(memory.Size == 0 || memory.PageFaults == 0) {
        fmt.Errorf("Error getting process memory after gather")
    }
}

func TestProcessCpu(test *testing.T) {
    testPid := os.Getpid()
    cpu := GetProcessCpu(uint64(testPid))

    if(cpu.User == 0 || cpu.Sys == 0 || 
        cpu.Total == 0 || cpu.Percent == 0) {
	fmt.Errorf("Error getting process cpu")
    }
    time.Sleep(2000 * time.Millisecond)
    cpu.Gather()
    if(cpu.User == 0 || cpu.Sys == 0 || 
        cpu.Total == 0 || cpu.Percent == 0) {
	fmt.Errorf("Error getting process cpu using gather")
    }
}


func TestProcessExe(test *testing.T) {
    testPid := os.Getpid()
    exe := GetProcessExe(uint64(testPid))
    if(len(exe) <= 0 ) {
	fmt.Errorf("Error getting process name")
        
    }
}
