package main

import (
    "fmt"
    //"time"
    //"path/filepath"
)

type Server struct {


}

func updateProcessList(processMap map[uint64]*ProcessInfo) {

    pidList := ProcessList()
    for _ , pid := range(pidList) {
        elem, ok := processMap[uint64(pid)]
	if(!ok) {
	    processMap[pid] = GetProcessInfo(pid)    
	} else {
	    elem.Update()
	}
    }
}

func main() {

    for {
        cpu := GetSystemCpuByInterval(5)
	memory := GetSystemMemory()
        fmt.Printf("Cpu - %4.3f %% %8.2f %% Used, %8.2f %% Free\n", cpu, memory.Used_percent, memory.Free_percent) 
    }

    /*
    pidMap := make(map[uint64]*ProcessInfo)

    updateProcessList(pidMap)
    time.Sleep(5 * time.Second)

    for {
        updateProcessList(pidMap)
	for pid, proc := range(pidMap) {
	    if(proc.Cpu.Percent > 0) {
                fmt.Printf("%8d : %30s - %6.3v %% - %6.3v MB\n",pid, filepath.Base(proc.Name), proc.Cpu.Percent, proc.Memory.Size) 
	    }
	}
        time.Sleep(5 * time.Second)
    }
    */

}


